Template.first.helpers({
    "posts": function () {
        return Posts.find({}, {
            sort: {
                date: 1
            }
        }).fetch();
    },
    "usernames": function () {
        return Meteor.users.find({}).fetch();
    }
});

Template.friends.helpers({
    friends: function () {
        if (Meteor.user().profile) {
            var friends = Meteor.user().profile.friends;
            var friendsList = [];

            for (var i = 0; i < friends.length; i++) {
                friendsList.push(Meteor.users.findOne(friends[i]));
            }

            return friendsList;
        }
    }
});