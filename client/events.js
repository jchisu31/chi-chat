Template.first.onCreated(function () {
    Meteor.subscribe('posts');
    Meteor.subscribe('users');
});

Template.first.events({
    "submit .post-form": function () {
        event.preventDefault();
        var name = event.target.userpost.value;
        var username = Meteor.user();
        var date = moment().format("dddd, MMMM Do , h:mm:ss a");

        Posts.insert({
            name: name,
            username: Meteor.user().username,
            date: date
        });
    }
});