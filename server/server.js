Meteor.publish('posts', function () {
    if (this.userId) {
        return Posts.find();
    }
});

Meteor.publish('users', function () {
    return Users.find();
});